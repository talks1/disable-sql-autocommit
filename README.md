# Disabling auto commit in sql server studio

## Via syntax
```
BEGIN TRANSACTIOn



ROLLBACK TRANSACTION
```

## View tools menu 

![](./autocommit.png)